//Import course model
const { default: mongoose } = require('mongoose');
const productTypeModel = require('../models/productTypeModel');

//Tao cac function crud
//get all productType
const getAllProductType = (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    productTypeModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(200).json({
                    status: "Get all productType sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//get one product type
const getProductTypeById = (req, res) => {
    //B1: thu thập dữ liệu
    var productTypeId = req.params.productTypeId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model
    productTypeModel.findById(productTypeId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Get productType by id sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any productType",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//create a product type
const createProductType = (req, res) => {
    //B1: thu thập dữ liệu
    const { name, description } = req.body;

    //B2: kiểm tra dữ liệu
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required!"
        })
    }
    if (!description) {
        return res.status(400).json({
            status: "Bad request",
            message: "description is required!"
        })
    }

    //B3: thực hiện thao tác model
    let newProductType = {
        _id: new mongoose.Types.ObjectId(),
        name,
        description
    }

    productTypeModel.create(newProductType)
        .then((data) => {
            return res.status(201).json({
                status: "Create new product type sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//update productType
const updateProductType = (req, res) => {
    //B1: thu thập dữ liệu
    var productTypeId = req.params.productTypeId;
    const { name, description } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required!"
        })
    }
    if (!description) {
        return res.status(400).json({
            status: "Bad request",
            message: "description is required!"
        })
    }
    //B3: thực thi model
    let updateProductType = {
        name,
        description
    }

    productTypeModel.findByIdAndUpdate(productTypeId, updateProductType)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Update product type sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any productType",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}
//delete product type
const deleteProductType = (req, res) => {
    //B1: Thu thập dữ liệu
    var productTypeId = req.params.productTypeId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    productTypeModel.findByIdAndDelete(productTypeId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Delete productType sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any productType",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

//Export thanh module
module.exports = {
    getAllProductType,
    getProductTypeById,
    createProductType,
    updateProductType,
    deleteProductType
}