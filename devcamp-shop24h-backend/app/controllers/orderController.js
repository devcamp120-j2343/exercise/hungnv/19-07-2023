//import thư viện mongoose
const mongoose = require('mongoose');

// import review model
const orderModel = require('../models/orderModel');

// import course model
const customerModel = require('../models/customerModel');

// const create order
const createOrder = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        customerId,
        orderDate,
        shippedDate,
        note,
        cost
    } = req.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Customer ID is not valid"
        })
    }
    if (orderDate && isNaN(new Date(orderDate))) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Invalid orderDate"
        });
    }

    if (shippedDate && isNaN(new Date(shippedDate))) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Invalid shippedDate"
        });
    }

    if (typeof cost !== "number" || cost < 0) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Invalid cost"
        });
    }
    if (!note) {
        return res.status(400).json({
            status: "Bad request",
            message: "note is required!"
        })
    }


    // B3: Thao tác với CSDL
    var newOrder = {
        _id: new mongoose.Types.ObjectId(),
        customerId: customerId,
        orderDate: orderDate,
        shippedDate: shippedDate,
        note: note,
        cost: cost
    }

    try {
        const createdOrder = await orderModel.create(newOrder);

        const updatedCustomer = await customerModel.findByIdAndUpdate(customerId, {
            $push: { orders: createdOrder._id }
        });

        return res.status(201).json({
            status: "Create order successfully",
            customer: updatedCustomer,
            data: createdOrder
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: err.message
        })
    }
}
//get all orders
const getAllOrders = async (req, res) => {
    //B1: thu thập dữ liệu
    const customerId = req.query.customerId;
    //B2: kiểm tra
    if (customerId !== undefined && !mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Customer ID is not valid"
        })
    }

    //B3: thực thi model
    try {
        if (customerId === undefined) {
            const orderList = await orderModel.find();

            if (orderList && orderList.length > 0) {
                return res.status(200).json({
                    status: "Get all orders sucessfully",
                    data: orderList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any order"
                })
            }
        } else {
            const customerInfo = await customerModel.findById(customerId).populate("customers");

            return res.status(200).json({
                status: "Get all orders of users  sucessfully",
                data: customerInfo.orders
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//get a order
const getOrderById = async (req, res) => {
    //B1: thu thập dữ liệu
    var orderId = req.params.orderId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    //B3: thực thi model
    try {
        const orderInfo = await orderModel.findById(orderId);

        if (orderInfo) {
            return res.status(200).json({
                status: "Get order by id sucessfully",
                data: orderInfo
            })
        } else {
            return res.status(404).json({
                status: "Not found any order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//update order
const updateOrder = async (req, res) => {
    //B1: thu thập dữ liệu
    var orderId = req.params.orderId;

    const { orderDate, shippedDate, note, cost } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (orderDate && isNaN(new Date(orderDate))) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Invalid orderDate"
        });
    }

    if (shippedDate && isNaN(new Date(shippedDate))) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Invalid shippedDate"
        });
    }

    if (typeof cost !== "number" || cost < 0) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Invalid cost"
        });
    }
    if (!note) {
        return res.status(400).json({
            status: "Bad request",
            message: "note is required!"
        })
    }

    //B3: thực thi model
    try {
        let updateOrder = {
            orderDate,
            shippedDate,
            note,
            cost
        }

        const updatedOrder = await orderModel.findByIdAndUpdate(customerId, updateOrder);

        if (updatedOrder) {
            return res.status(200).json({
                status: "Update order sucessfully",
                data: updatedOrder
            })
        } else {
            return res.status(404).json({
                status: "Not found any order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//delete order
const deleteOrder = async (req, res) => {
    //B1: Thu thập dữ liệu
    var orderId = req.params.orderId;
    // Nếu muốn xóa id thừa trong mảng order thì có thể truyền thêm customerId để xóa
    var customerId = req.query.customerId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    try {
        const deletedOrder = await orderModel.findByIdAndDelete(orderId);

        // Nếu có courseId thì xóa thêm (optional)
        if (orderId !== undefined) {
            await userModel.findByIdAndUpdate(customerId, {
                $pull: { orders: orderId }
            })
        }

        if (deletedOrder) {
            return res.status(200).json({
                status: "Delete order sucessfully",
                data: deletedOrder
            })
        } else {
            return res.status(404).json({
                status: "Not found any order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createOrder,
    getAllOrders,
    getOrderById,
    updateOrder,
    deleteOrder
}