//Import course model
const { default: mongoose } = require('mongoose');
const productModel = require('../models/productModel');

//Tao cac function crud
//get all product
//get all productType
const getAllProduct = (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    productModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(200).json({
                    status: "Get all product sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any product",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//get one product 
const getProductById = (req, res) => {
    //B1: thu thập dữ liệu
    var productId = req.params.productId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model
    productModel.findById(productId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Get product by id sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any product",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//create a product 
const createProduct = (req, res) => {
    //B1: thu thập dữ liệu
    const { name, description,type,imageUrl,buyPrice,promotionPrice,amount } = req.body;

    //B2: kiểm tra dữ liệu
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required!"
        })
    }
    if (!description) {
        return res.status(400).json({
            status: "Bad request",
            message: "description is required!"
        })
    }
    if (!imageUrl) {
        return res.status(400).json({
            status: "Bad request",
            message: "imageUrl is required!"
        })
    }
    if (typeof buyPrice !== "number" || buyPrice < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "buyPrice is invalid!"
        });
    }

    if (typeof promotionPrice !== "number" || promotionPrice < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "promotionPrice is invalid!"
        });
    }

    if (typeof amount !== "number" || amount < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "amount is invalid!"
        });
    }

    //B3: thực hiện thao tác model
    let newProduct = {
        _id: new mongoose.Types.ObjectId(),
        name,
        description,
        type,
        imageUrl,
        buyPrice,
        promotionPrice,
        amount
    }

    productModel.create(newProduct)
        .then((data) => {
            return res.status(201).json({
                status: "Create new product sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//update product
const updateProduct = (req, res) => {
    //B1: thu thập dữ liệu
    var productId = req.params.productId;
    const { name, description,imageUrl,buyPrice,promotionPrice,amount } = req.body;

    //B2: kiểm tra dữ liệu
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required!"
        })
    }
    if (!description) {
        return res.status(400).json({
            status: "Bad request",
            message: "description is required!"
        })
    }
    if (!imageUrl) {
        return res.status(400).json({
            status: "Bad request",
            message: "imageUrl is required!"
        })
    }
    if (typeof buyPrice !== "number" || buyPrice < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "buyPrice is invalid!"
        });
    }

    if (typeof promotionPrice !== "number" || promotionPrice < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "promotionPrice is invalid!"
        });
    }

    if (typeof amount !== "number" || amount < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "amount is invalid!"
        });
    }
    //B3: thực thi model
    let updateProduct = {
        name,
        description,
        imageUrl,
        buyPrice,
        promotionPrice,
        amount
    }

    productModel.findByIdAndUpdate(productId, updateProduct)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Update product sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any productType",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}
//delete product 
const deleteProduct = (req, res) => {
    //B1: Thu thập dữ liệu
    var productId = req.params.productId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    productModel.findByIdAndDelete(productId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Delete product sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any product",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}
//Export thanh module
module.exports = {
    getAllProduct,
    getProductById,
    createProduct,
    updateProduct,
    deleteProduct
}