//Import course model
const { default: mongoose } = require('mongoose');
const customerModel = require('../models/customerModel');

//Tao cac function crud

//get all customer
const getAllCustomer = (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    customerModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(200).json({
                    status: "Get all customer sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any customer",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//get one customer 
const getCustomerById = (req, res) => {
    //B1: thu thập dữ liệu
    var customerId = req.params.customerId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model
    customerModel.findById(customerId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Get customer by id sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any customer",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//create a customer 
const createCustomer = (req, res) => {
    //B1: thu thập dữ liệu
    const { fullName, phone,email,address,city,country } = req.body;

    //B2: kiểm tra dữ liệu
    if (!fullName) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required!"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required!"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required!"
        })
    }
    if (!address) {
        return res.status(400).json({
            status: "Bad request",
            message: "address is required!"
        })
    }
    if (!city) {
        return res.status(400).json({
            status: "Bad request",
            message: "city is required!"
        })
    }
    if (!country) {
        return res.status(400).json({
            status: "Bad request",
            message: "country is required!"
        })
    }
    

    //B3: thực hiện thao tác model
    let newCustomer = {
        _id: new mongoose.Types.ObjectId(),
        fullName, 
        phone,
        email,
        address,
        city,
        country
    }

    customerModel.create(newCustomer)
        .then((data) => {
            return res.status(201).json({
                status: "Create new customer sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//update customer
const updateCustomer = (req, res) => {
    //B1: thu thập dữ liệu
    var customerId = req.params.customerId;
    const { fullName, phone,email,address,city,country } = req.body;

    //B2: kiểm tra dữ liệu
    if (!fullName) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required!"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required!"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required!"
        })
    }
    if (!address) {
        return res.status(400).json({
            status: "Bad request",
            message: "address is required!"
        })
    }
    if (!city) {
        return res.status(400).json({
            status: "Bad request",
            message: "city is required!"
        })
    }
    if (!country) {
        return res.status(400).json({
            status: "Bad request",
            message: "country is required!"
        })
    }
    //B3: thực thi model
    let updateCustomer = {
        fullName, 
        phone,
        email,
        address,
        city,
        country
    }

    customerModel.findByIdAndUpdate(customerId, updateCustomer)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Update customer sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any customer",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}
//delete customer 
const deleteCustomer = (req, res) => {
    //B1: Thu thập dữ liệu
    var customerId = req.params.customerId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    customerModel.findByIdAndDelete(customerId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Delete customer sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any customer",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}
//Export thanh module
module.exports = {
    getAllCustomer,
    getCustomerById,
    createCustomer,
    updateCustomer,
    deleteCustomer
}