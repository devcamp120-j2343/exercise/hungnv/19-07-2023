//Import thu vien express
const express = require("express");
//Khai bao middleware
const productTypeMiddleware = require("../middleware/productTypeMiddleware");
//import controller
const { getAllProductType, createProductType, getProductTypeById, updateProductType, deleteProductType } = require("../controllers/productTypeController");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL ProductType: ", req.url);

    next();
});
//get all ProductType
router.get("/productTypes", productTypeMiddleware.getAllProductTypeMiddleware, getAllProductType)
//create ProductType
router.post("/productTypes", productTypeMiddleware.postProductTypeMiddleware, createProductType);
//get a ProductType
router.get("/productTypes/:productTypeId", productTypeMiddleware.getAProductTypeMiddleware, getProductTypeById);
//update ProductType
router.put("/productTypes/:productTypeId", productTypeMiddleware.putProductTypeMiddleware, updateProductType);
//delete ProductType
router.delete("/productTypes/:productTypeId", productTypeMiddleware.deleteProductTypeMiddleware, deleteProductType);

module.exports = router;