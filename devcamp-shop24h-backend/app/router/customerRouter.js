//Import thu vien express
const express = require("express");
//Khai bao middleware
const customerMiddleware = require("../middleware/customerMiddleware");
//import controller
const { getAllCustomer, createCustomer, getCustomerById, updateCustomer, deleteCustomer } = require("../controllers/customerController");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL Customer: ", req.url);

    next();
});
//get all customer
router.get("/customers", customerMiddleware.getAllCustomertMiddleware, getAllCustomer)
//create customer
router.post("/customers", customerMiddleware.postCustomertMiddleware, createCustomer);
//get a customer
router.get("/customers/:customerId", customerMiddleware.getACustomertMiddleware, getCustomerById);
//update customer
router.put("/customers/:customerId", customerMiddleware.putCustomertMiddleware, updateCustomer);
//delete customer
router.delete("/customers/:customerId", customerMiddleware.deleteCustomertMiddleware, deleteCustomer);

module.exports = router;