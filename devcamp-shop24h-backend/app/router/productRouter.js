//Import thu vien express
const express = require("express");
//Khai bao middleware
const productMiddleware = require("../middleware/productMiddleware");
//import controller
const { getAllProduct, createProduct, getProductById, updateProduct, deleteProduct } = require("../controllers/productController");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL Product: ", req.url);

    next();
});
//get all Product
router.get("/products", productMiddleware.getAllProductMiddleware, getAllProduct)
//create ProductType
router.post("/products", productMiddleware.postProductMiddleware, createProduct);
//get a ProductType
router.get("/products/:productId", productMiddleware.getAProductMiddleware, getProductById);
//update ProductType
router.put("/products/:productId", productMiddleware.putProductMiddleware, updateProduct);
//delete ProductType
router.delete("/products/:productId", productMiddleware.deleteProductMiddleware, deleteProduct);

module.exports = router;