//Import thu vien express
const express = require("express");
//Khai bao middleware
const orderMiddleware = require("../middleware/orderMiddleware");
//import controller
const { getAllOrders, createOrder, getOrderById, updateOrder, deleteOrder } = require("../controllers/orderController");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL order: ", req.url);

    next();
});
//get all order
router.get("/orders", orderMiddleware.getAllOrdertMiddleware,getAllOrders )
//create order
router.post("/customers/:customerId/orders", orderMiddleware.postOrdertMiddleware,createOrder );
//get a order
router.get("/orders/:orderId", orderMiddleware.getAOrdertMiddleware,getOrderById );
//update order
router.put("/orders/:orderId", orderMiddleware.putOrdertMiddleware,updateOrder );
//delete order
router.delete("/orders/:orderId", orderMiddleware.deleteOrdertMiddleware,deleteOrder );

module.exports = router;