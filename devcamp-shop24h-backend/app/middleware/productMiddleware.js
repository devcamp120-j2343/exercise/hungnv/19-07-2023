const getAllProductMiddleware = (req, res, next) => {
    console.log("Get All Product!");
    next();
}

const getAProductMiddleware = (req, res, next) => {
    console.log("Get a Product!");
    next();
}
const postProductMiddleware = (req, res, next) => {
    console.log("Create a Product!");
    next();
}

const putProductMiddleware = (req, res, next) => {
    console.log("Update a Product!");
    next();
}
const deleteProductMiddleware = (req, res, next) => {
    console.log("Delete a Product!");
    next();
}

//export
module.exports = {
    getAllProductMiddleware,
    getAProductMiddleware,
    postProductMiddleware,
    putProductMiddleware,
    deleteProductMiddleware
}