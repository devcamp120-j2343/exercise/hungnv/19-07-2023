const getAllProductTypeMiddleware = (req, res, next) => {
    console.log("Get All ProductType!");
    next();
}

const getAProductTypeMiddleware = (req, res, next) => {
    console.log("Get a ProductType!");
    next();
}
const postProductTypeMiddleware = (req, res, next) => {
    console.log("Create a ProductType!");
    next();
}

const putProductTypeMiddleware = (req, res, next) => {
    console.log("Update a ProductType!");
    next();
}
const deleteProductTypeMiddleware = (req, res, next) => {
    console.log("Delete a ProductType!");
    next();
}

//export
module.exports = {
    getAllProductTypeMiddleware,
    getAProductTypeMiddleware,
    postProductTypeMiddleware,
    putProductTypeMiddleware,
    deleteProductTypeMiddleware
}