const getAllCustomertMiddleware = (req, res, next) => {
    console.log("Get All Customert!");
    next();
}

const getACustomertMiddleware = (req, res, next) => {
    console.log("Get a Customert!");
    next();
}
const postCustomertMiddleware = (req, res, next) => {
    console.log("Create a Customert!");
    next();
}

const putCustomertMiddleware = (req, res, next) => {
    console.log("Update a Customert!");
    next();
}
const deleteCustomertMiddleware = (req, res, next) => {
    console.log("Delete a Customert!");
    next();
}

//export
module.exports = {
    getAllCustomertMiddleware,
    getACustomertMiddleware,
    postCustomertMiddleware,
    putCustomertMiddleware,
    deleteCustomertMiddleware
}