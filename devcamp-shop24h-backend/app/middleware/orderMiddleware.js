const getAllOrdertMiddleware = (req, res, next) => {
    console.log("Get All Ordert!");
    next();
}

const getAOrdertMiddleware = (req, res, next) => {
    console.log("Get a Ordert!");
    next();
}
const postOrdertMiddleware = (req, res, next) => {
    console.log("Create a Ordert!");
    next();
}

const putOrdertMiddleware = (req, res, next) => {
    console.log("Update a Ordert!");
    next();
}
const deleteOrdertMiddleware = (req, res, next) => {
    console.log("Delete a Ordert!");
    next();
}

//export
module.exports = {
    getAllOrdertMiddleware,
    getAOrdertMiddleware,
    postOrdertMiddleware,
    putOrdertMiddleware,
    deleteOrdertMiddleware
}