//B1: Khai bao thu vien mongoose
const mongoose = require('mongoose');

//B2: Khai bao thu vien Schema cua mongoose
const Schema = mongoose.Schema;

//B3: Tao doi tuong Schema bao gom cac thuoc tinh cua collection
const orderSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    orderDate: {
        type: Date,
        default: Date.now()
    },
    shippedDate: {
        type: Date
    },
    note: {
        type: String

    },
    orderDetails: [{
        type: mongoose.Types.ObjectId,
        ref: 'orderDetail'
    }],
    cost: {
        type: Number,
        default: 0
    }
});
//B4: export Schema ra model
module.exports = mongoose.model("order", orderSchema);