//B1: Khai bao thu vien mongoose
const mongoose = require('mongoose');

//B2: Khai bao thu vien Schema cua mongoose
const Schema = mongoose.Schema;

//B3: Tao doi tuong Schema bao gom cac thuoc tinh cua collection
const productTypeSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        unique: true,
        require: true
    },
    description: {
        type: String
    }
});
//B4: export Schema ra model
module.exports = mongoose.model("productType", productTypeSchema);