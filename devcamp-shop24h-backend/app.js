const express = require("express"); // Tương tự : import express from "express";
const path = require("path");
//Import thu vien mongoose
const mongoose = require('mongoose');
//Khai bao model mongoose
const productTypeModel = require('./app/models/productTypeModel');
const productModel = require('./app/models/productModel');
const customerModel = require('./app/models/customerModel');
const orderModel = require('./app/models/orderModel');
//Khai bao router
const productTypeRouter = require("./app/router/productTypeRouter");
const productRouter = require("./app/router/productRouter");
const customerRouter = require("./app/router/customerRouter");
const orderRouter = require("./app/router/orderRouter");
// Khởi tạo Express App
const app = express();
// Kết nối với MongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Shop24h")
    .then(() => {
        console.log('Successfully connected to MongooseDB');
    })
    .catch((error) => {
        throw error;
    });
// Khai báo ứng dụng đọc được body raw json (Build in middleware)
app.use(express.json());

//Khai bao ung dung se chay cong 8000
const port = 8000;

app.use("/api", productTypeRouter);
app.use("/api", productRouter);
app.use("/api", customerRouter);
app.use("/api", orderRouter);

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})
